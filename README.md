# icon-boost
Replacing macOS app icons

### Installation
Link the icon-boost command to usr/local/bin:

```zsh
ln -s /<full>/<path>/<to>/icon-boost.sh /usr/local/bin/icon-boost
```

### Configuration
The configuration of apps for which to replace icons is done using the "applications.json" file. Simply add new apps to the list by specifying their keys. Additionally, you can set three properties for each app:
```json
"signal": {
	"app_path": "/Applications/Signal.app",
	"original_icon_path": "Signal.icns",
	"icon_path": "/[path]/[to]/icon-boost/icons/signal.icns"
}
```

- **app_path**: The path to your application. If not set, '/Applications/[key].app' will be used, while [key] is the key you specified before (dashes replaced with spaces).
- **original_icon_path**: The path to the icon that should be replaced, after the prefix "[app_path]/Contents/Resources". Usually, this is just a file name. If not set, the script will try to detect the icon by itself.
- **icon_path**: The path to the new icon. If not set, the script will search the "icons" folder for an .icns file with the key as its name.

### Credits
Follows the approach by Seth Vargo to [changing macOS application icons programmatically](https://www.sethvargo.com/replace-icons-osx/).
