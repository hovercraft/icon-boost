require 'json'

CONFIG_PATH = File.join(File.expand_path(__dir__), "applications.json")

def app_quarantined?(app_path)
  xattr = `xattr #{app_path.gsub(" ", "\\ ")}`
  if !xattr.include?("com.apple.quarantine")
    return false
  end
  
  # see: https://stackoverflow.com/questions/59974353/checking-if-macos-app-has-ever-been-un-quarantined-and-fully-launched
  # see: https://eclecticlight.co/2017/08/15/quarantined-more-about-the-quarantine-extended-attribute/
  xattr_p = `xattr -p com.apple.quarantine #{app_path.gsub(" ", "\\ ")}`
  gatekeeper_score = xattr_p[0..3].to_i(16)
  return (gatekeeper_score & 0b01000000) == 0
end

applications = JSON.parse(File.read(CONFIG_PATH), symbolize_names: true)

count = 0

applications.each do |key, attributes|  
  app_path = attributes[:app_path] ? attributes[:app_path] : "/Applications/#{key.to_s.gsub("-", " ")}.app"
  if !File.exist?(app_path)
    puts "App for key '#{key}' does not exist (path: #{app_path}). Skipping..."
    next
  end
  
  if attributes[:original_icon_path]
    original_icon_path = File.join(app_path, "Contents/Resources", attributes[:original_icon_path])
    if !File.exist?(original_icon_path)
      puts "There is no icon to replace for app '#{key}' (path: #{original_icon_path}). Skipping..."
      next
    end
  else
    potential_original_image_paths = Dir.glob(File.join(app_path, "/Contents/Resources/*.icns"))
    if potential_original_image_paths.size != 1
      puts "The icon to replace for app '#{key}' can not be detected automatically. Please specify it manually in the config file. Skipping..."
      next
    end
    original_icon_path = potential_original_image_paths[0]
  end
  
  icon_path = attributes[:icon_path] ? attributes[:icon_path] : File.join(File.expand_path(__dir__), "icons", "#{key}.icns")
  if !File.exist?(icon_path)
    puts "Replacement icon for app '#{key}' does not exist (path: #{icon_path}). Skipping..."
    next
  end
  
  if File.size(icon_path) == File.size(original_icon_path)
    next
  end
  
  
  if !attributes[:ignore_quarantine] and !app_quarantined?(app_path)
    `open #{app_path.gsub(" ", "\\ ")}`
    i = 0
    begin
      if i >= 5
        puts "#{key} is still quarantined. Please resolve this, then re-run the script."
        exit
      end
      puts "#{key} is quarantined, please click \"open\" in the dialogue to continue."
      sleep(4)
      i += 1
    end while app_quarantined?(app_path)
  end
  
  print `sudo cp #{icon_path.gsub(" ", "\\ ")} #{original_icon_path.gsub(" ", "\\ ")}`
  `sudo touch #{app_path.gsub(" ", "\\ ")}`
  puts "Replaced #{key} icon."
  
  count += 1
end

if count > 0
  `sudo killall Dock`
end

puts (count == 1 ? "Replaced 1 icon in total." : "Replaced #{count} icons in total.")
